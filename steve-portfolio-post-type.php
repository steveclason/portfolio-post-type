<?php
/*
Plugin Name:  Steve's Portfolio Post Type
Plugin URI:   https://bitbucket.org/steveclason/portfolio-post-type/
Description:  Adds a Portfolio post-type to WordPress, with a taxonomy suited to Web developers.
Version:     1.0
Author:       Steve Clason
Author URI:   https://steveclason.com
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:
Domain Path:
*/

/**
 * Portfolio Custom Post Type
 */

function theme_register_post_type_portfolio() {
	register_post_type( 'portfolio',
		array(
			'labels' => array(
				'name' => __( 'Portfolio' ),
				'singular_name' => __( 'Portfolio Item' ),
				'new_item' => __('New Portfolio Item'),
				'add_new_item' => __('Add New Portfolio Item')
			),
		'menu_name' => "Portfolio",
    'menu_icon' => 'dashicons-portfolio',
		'description' => 'Portfolio item with thumbnail and skills listing.',
		'public' => true,
		'has_archive' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'supports' => array(
			'title',
			'thumbnail',
			'editor',
			'custom-fields',
			'revisions',
			'page-attributes',
			'post-formats',
			'wpcom-markdown'
			)
		)
	);
}

add_action( 'init', 'theme_register_post_type_portfolio' );


function portfolio_url(){
	global $post;
	$custom = get_post_custom( $post -> ID );
  if ( $custom && $custom["portfolio-url"][0] ) {
	  $portfolio_url = $custom["portfolio-url"][0];
  } else {
    $portfolio_url = '';
  }

	?>
		<label>URL:</label>
		<input name="portfolio-url" value="<?php echo $portfolio_url; ?>" />
	<?php
}

function portfolio_name(){
	global $post;
	$custom = get_post_custom( $post -> ID );
  if ( $custom && $custom["portfolio-name"][0] ) {
	  $portfolio_name = $custom["portfolio-name"][0];
  } else {
    $portfolio_name = '';
  }
	?>
		<label>Name (Client):</label>
		<input name="portfolio-name" value="<?php echo $portfolio_name; ?>" />
	<?php
}

function portfolio_date(){
	global $post;
	$custom = get_post_custom( $post -> ID );
  if ( $custom && $custom["portfolio-date"][0] ) {
    $portfolio_date = $custom["portfolio-date"][0];
  } else {
    $portfolio_date = '';
  }
	?>
		<label>Date:</label>
		<input name="portfolio-date" value="<?php echo $portfolio_date; ?>" />
	<?php
}

function portfolio_designer(){
	global $post;
	$custom = get_post_custom( $post -> ID );
  if ( $custom && $custom["portfolio-designer"][0] ) {
    $portfolio_designer = $custom["portfolio-designer"][0];
  } else {
    $portfolio_designer = '';
  }
	?>
		<label>Designer:</label>
		<input name="portfolio-designer" value="<?php echo $portfolio_designer; ?>" />
	<?php
}

function portfolio_manager(){
	global $post;
	$custom = get_post_custom( $post -> ID );
  if ( $custom && $custom["portfolio-manager"][0] ) {
    $portfolio_manager = $custom["portfolio-manager"][0];
  } else {
    $portfolio_manager = '';
  }
	?>
		<label>Project Manager:</label>
		<input name="portfolio-manager" value="<?php echo $portfolio_manager; ?>" />
	<?php
}

function portfolio_init(){
	//
	add_meta_box( "portfolio_url-meta", "Portfolio Item URL", "portfolio_url", "portfolio", "normal", "low" );
	add_meta_box( "portfolio_name-meta", "Portfolio Item Name", "portfolio_name", "portfolio", "normal", "low" );
	add_meta_box( "portfolio-date-meta", "Portfolio Item Date", "portfolio_date", "portfolio", "normal", "low" );
	add_meta_box( "portfolio_designer-meta", "Portfolio Item Designer", "portfolio_designer", "portfolio", "normal", "low" );
	add_meta_box( "portfolio_manager-meta", "Portfolio Item Project Manager", "portfolio_manager", "portfolio", "normal", "low" );
}

add_action("admin_init", "portfolio_init");

function theme_register_skills_taxonomy() {
	$labels = array(
		'name'                       => _x( 'Skills', 'taxonomy general name' ),
		'singular_name'              => _x( 'Skill', 'taxonomy singular name' ),
		'search_items'               => __( 'Search Skills' ),
		'popular_items'              => __( 'Popular Skills' ),
		'all_items'                  => __( 'All Skills' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Skill' ),
		'update_item'                => __( 'Update Skill' ),
		'add_new_item'               => __( 'Add New SKill' ),
		'new_item_name'              => __( 'New Skill' ),
		'separate_items_with_commas' => __( 'Separate skills with commas' ),
		'add_or_remove_items'        => __( 'Add or remove skills' ),
		'choose_from_most_used'      => __( 'Choose from the most used skills' ),
		'not_found'                  => __( 'No skills found.' ),
		'menu_name'                  => __( 'Skills' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'skill' ),
	);

	register_taxonomy( 'skills', 'portfolio', $args );
}

add_action( 'init', 'theme_register_skills_taxonomy', 0 );




// Save Portfolio Items Duh.
function save_portfolio(){
	global $post;
	if( $post && $post->post_type === 'portfolio' ) {
		update_post_meta($post->ID, "portfolio-url", $_POST["portfolio-url"]);
		update_post_meta($post->ID, "portfolio-name", $_POST["portfolio-name"]);
		update_post_meta($post->ID, "portfolio-date", $_POST["portfolio-date"]);
		update_post_meta($post->ID, "portfolio-designer", $_POST["portfolio-designer"]);
		update_post_meta($post->ID, "portfolio-manager", $_POST["portfolio-manager"]);
	}
}

add_action('save_post', 'save_portfolio');

/* End Portfolio Custom Post Type */
